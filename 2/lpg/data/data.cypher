CREATE (a:Game {name: 'Doom', rating: '10', Comments: ''})
CREATE (b:Game:DLC {name: 'DLC'})
WITH a, b
MATCH (a:Game), (b:Game) WHERE a.name = 'Doom' AND b.name = 'DLC' 
CREATE (a)-[r:Contains]->(b)
WITH a, b
MATCH (a:Game), (b:Game) WHERE a.name = 'Doom' AND b.name = 'DLC' 
CREATE (b)-[r:Partof]->(a) 
CREATE (c:Game:Publisher {name: 'Activision'})
WITH a, b, c
MATCH (a:Game), (c:Game) WHERE a.name = 'Doom' AND c.name = 'Activision' 
CREATE (a)-[r:Publisher{date: "1999-08-07"}]->(c) 
CREATE (d:Game:Genre {name: 'FPS'})
WITH a, b, c, d
MATCH (a:Game), (d:Game) WHERE a.name = 'Doom' AND d.name = 'FPS'
CREATE (d)-[r:Partof]->(a)
CREATE (e:Game:Platform {name: 'N64'})
WITH a, b, c, d, e
MATCH (a:Game), (e:Game) WHERE a.name = 'Doom' AND e.name = 'N64' 
CREATE (e)-[r:Partof]->(a)
WITH a, b, c, d, e
MATCH (a:Game), (e:Game) WHERE a.name = 'Doom' AND e.name = 'N64' 
CREATE (a)-[r:Runson]->(e)
CREATE (f:Steam:Price {name: 'Price', value: '10', currency: 'CZK'})
WITH a, b, c, d, e, f
MATCH (f:Steam:Price), (a:Game) WHERE a.name = 'Doom' AND f.name = 'Price' 
CREATE (f)-[r:Partof]->(a)
CREATE (g:Software {name: 'Paint', rating: '10'})
WITH a, b, c, d, e, f, g
MATCH (g:Software), (f:Price) WHERE g.name = 'Paint' AND f.name = 'Price' 
CREATE (f)-[r:Partof]->(g)
WITH a, b, c, d, e, f, g
MATCH (g:Software), (c:Game) WHERE g.name = 'Paint' AND c.name = 'Activision' 
CREATE (g)-[r:Publisher{date: "2001-01-01"}]->(c)
CREATE (h:Requirement {name: 'Requirement', Minval: '0', recVal: '1'})
CREATE (i:Requirement {name: 'GPU'})
CREATE (j:Requirement {name: 'CPU'})
CREATE (k:Requirement {name: 'Memory'})
WITH a, b, c, d, e, f, g, h, i, j, k
MATCH (h:Requirement), (i:Requirement) WHERE h.name = 'Requirement' AND j.name = 'CPU' 
CREATE (h)-[r:Contains]->(j)
WITH a, b, c, d, e, f, g, h, i, j, k
MATCH (h:Requirement), (i:Requirement) WHERE h.name = 'Requirement' AND i.name = 'GPU' 
CREATE (h)-[r:Contains]->(i)
WITH a, b, c, d, e, f, g, h, i, j, k
MATCH (h:Requirement), (i:Requirement) WHERE h.name = 'Requirement' AND k.name = 'Memory' 
CREATE (h)-[r:Contains]->(k)
CREATE (l:Library {name: 'Library'})
WITH a, b, c, d, e, f, g, h, i, j, k, l
MATCH (a:Game), (l:Library) WHERE a.name = 'Doom' AND l.name = 'Library' 
CREATE (l)-[r:Contains {since: "2006/6/6"}]->(a)
WITH a, b, c, d, e, f, g, h, i, j, k, l
MATCH (g:Software), (l:Library) WHERE g.name = 'Paint' AND l.name = 'Library' 
CREATE (l)-[r:Contains {since: "2002/8/6"}]->(g)
CREATE (m:Account {name: 'Peter', email: "a@b.cz", password: "1234", adress: "abc 1", age: "36"})
WITH a, b, c, d, e, f, g, h, i, j, k, l, m
MATCH (m:Account), (l:Library) WHERE m.name = 'Peter' AND l.name = 'Library' 
CREATE (m)-[r:Owns]->(l)
WITH a, b, c, d, e, f, g, h, i, j, k, l, m
MATCH (m:Account), (a:Game) WHERE m.name = 'Peter' AND a.name = 'Doom' 
CREATE (m)-[r:Wishlisted{date: "2003-03-03", obtained:"Yes"}]->(a)
CREATE (n:Wallet {name: 'Wallet', balance: "12", currency: "CZK", card: "44568"})
WITH a, b, c, d, e, f, g, h, i, j, k, l, m, n
MATCH (m:Account), (n:Wallet) WHERE m.name = 'Peter' AND n.name = 'Wallet' 
CREATE (n)-[r:Partof]->(m)
CREATE (o:Review {name: 'Review', thumbs: "1", date: "2002-02-13", positive: "1", playtime:"99"})
WITH a, b, c, d, e, f, g, h, i, j, k, l, m, n, o
MATCH (o:Review), (a:Game) WHERE o.name = 'Review' AND a.name = 'Doom' 
CREATE (o)-[r:Partof]->(a)
WITH a, b, c, d, e, f, g, h, i, j, k, l, m, n, o
MATCH (o:Review), (m:Account) WHERE o.name = 'Review' AND m.name = 'Peter' 
CREATE (m)-[r:Wrote{date: "2011-11-11"}]->(o)
WITH a, h
MATCH (h:Requirement), (a:Game) WHERE h.name = 'Requirement' AND a.name = 'Doom' 
CREATE (h)-[r:Partof]->(a)
