//What game that Peter owns runs on the N64 platform.

MATCH ({name: "Peter"})-[:Owns]->(:Library)-[:Contains]->(Game)-[:Runson]->({name: "N64"})
RETURN Game