//Finds the shorterst path from Doom to Paint, which is of course the library they are both contained in.

MATCH
  (a:Game {name: 'Doom'}),
  (b:Software {name: 'Paint'}),
  p = shortestPath((a)-[*..5]-(b))
RETURN p
