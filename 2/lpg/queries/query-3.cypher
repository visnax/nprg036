//Lists the date, playtime and verdict of all reviews written by users about the game "Doom". 

MATCH (:Account)-[s:Wrote]->(m:Review)-[Partof]->({name: "Doom"}) 
return s.date, m.playtime, m.positive